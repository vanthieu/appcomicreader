package z79.trainingcourse.appcomicreadertwo.model;

public class LoginUserModel {

    private boolean success;
    private Users result;

    public LoginUserModel() {
    }

    public LoginUserModel(boolean success, Users result) {
        this.success = success;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Users getUsers() {
        return result;
    }

    public void setUsers(Users result) {
        this.result = result;
    }
}
