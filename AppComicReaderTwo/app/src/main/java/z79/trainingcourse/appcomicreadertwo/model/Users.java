package z79.trainingcourse.appcomicreadertwo.model;

import java.util.Date;

public class Users {

    private int ID;
    private String Unique_id;
    private String Email;
    private String UserName;
    private String UserPassword;
    private String Salt;
    private Date Create_at;
    private Date Update_at;

    public Users() {
    }

    public Users(int ID, String unique_id, String email, String userName, String userPassword, String salt, Date create_at, Date update_at) {
        this.ID = ID;
        Unique_id = unique_id;
        Email = email;
        UserName = userName;
        UserPassword = userPassword;
        Salt = salt;
        Create_at = create_at;
        Update_at = update_at;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getUnique_id() {
        return Unique_id;
    }

    public void setUnique_id(String unique_id) {
        Unique_id = unique_id;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getUserPassword() {
        return UserPassword;
    }

    public void setUserPassword(String userPassword) {
        UserPassword = userPassword;
    }

    public String getSalt() {
        return Salt;
    }

    public void setSalt(String salt) {
        Salt = salt;
    }

    public Date getCreate_at() {
        return Create_at;
    }

    public void setCreate_at(Date create_at) {
        Create_at = create_at;
    }

    public Date getUpdate_at() {
        return Update_at;
    }

    public void setUpdate_at(Date update_at) {
        Update_at = update_at;
    }
}
