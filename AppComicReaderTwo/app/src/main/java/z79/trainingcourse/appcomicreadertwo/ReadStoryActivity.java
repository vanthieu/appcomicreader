package z79.trainingcourse.appcomicreadertwo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import z79.trainingcourse.appcomicreadertwo.adapter.AdapterListComic;
import z79.trainingcourse.appcomicreadertwo.model.Chapter;
import z79.trainingcourse.appcomicreadertwo.model.Link;
import z79.trainingcourse.appcomicreadertwo.retrofit.APIComic;
import z79.trainingcourse.appcomicreadertwo.retrofit.RetrofitClient;
import z79.trainingcourse.appcomicreadertwo.roomdb.Content;
import z79.trainingcourse.appcomicreadertwo.roomdb.IStoryDataSource;
import z79.trainingcourse.appcomicreadertwo.roomdb.LocalStoryDataSource;
import z79.trainingcourse.appcomicreadertwo.roomdb.StoryDatabase;
import z79.trainingcourse.appcomicreadertwo.utils.Common;

public class ReadStoryActivity extends AppCompatActivity {

    @BindView(R.id.txt_title_readStory)
    TextView txt_title_readStory;
    @BindView(R.id.txt_content_readStory)
    TextView txt_content_readStory;
    @BindView(R.id.btn_back)
    Button btn_back;
    @BindView(R.id.btn_next)
    Button btn_next;
    @BindView(R.id.imgbtn_close_readStory)
    ImageButton imgbtn_close_readStory;

    @BindView(R.id.recycler_list_comic)
    RecyclerView recycler_list_comic;
    @BindView(R.id.layout_story)
    LinearLayout layout_story;
    @BindView(R.id.layout_comic)
    LinearLayout layout_comic;

    AdapterListComic adapterListComic;


    CompositeDisposable disposable;
    APIComic mservice;
    IStoryDataSource iStoryDataSource;
    List<Chapter> chapter;
    List<Link> linkList;
    int i = 0;
    int positionLinkList = 0;
    int chapterID = 0;
    private List<Content> linkListsFromDB;

    int IDFromDB = 0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_story);
        ButterKnife.bind(this);
        disposable = new CompositeDisposable();
        mservice = RetrofitClient.getRetrofit(Common.API_COMIC_ENPOINT).create(APIComic.class);
        iStoryDataSource = new LocalStoryDataSource(StoryDatabase.getInstance(this).iStoryDAO());

        linkList = new ArrayList<>();
        linkListsFromDB = new ArrayList<>();


        checkPermissionInternet();
        init();


    }

    private void init() {

//        eventLoadTitleStory();

//        eventBtnNextChap();
//        eventBtnPreviousChap();
        eventImgBtnClose();

    }


    private void eventImgBtnClose() {
        imgbtn_close_readStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }

    private void eventBtnPreviousChap() {
        btn_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                i--;
                updateTitle();

                chapterID = chapter.get(i).getID();
                eventLoadContentStoryOrComic(chapterID);
                Log.d("size",chapter.size() + "");
                Log.d("i",i + "");

                if (i <= 0) {
                    btn_back.setVisibility(View.INVISIBLE);
                }
//                btn_back.setVisibility(View.VISIBLE);
                btn_next.setVisibility(View.VISIBLE);

            }
        });
    }

    private void eventBtnNextChap() {

        btn_next.setOnClickListener(view -> {
            i++;
            updateTitle();

            chapterID = chapter.get(i).getID();
            Log.d("ChapterIdafter",chapterID + "");
            eventLoadContentStoryOrComic(chapterID);

            Log.d("ids",chapter.get(i).getID()+"");

            if (i >= chapter.size() - 1) {
                btn_next.setVisibility(View.INVISIBLE);
            }
            btn_back.setVisibility(View.VISIBLE);

        });
    }


    private void eventLoadTitleStory() {
        disposable.add(mservice.getChapterByStoryID(Common.currentIDStory)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(chapterModel -> {
                    if (chapterModel.isSuccess()) {
                        chapter = new ArrayList<>();
                        chapter = chapterModel.getResult();

                        updateTitle();

                        chapterID = chapter.get(i).getID();
                        Log.d("ChapterId",chapterID+ "");
                        Log.d("ChapterName",chapter.get(i).getName()+ "");
                        Log.d("ChapterSize",chapter.size() + "");
                        eventLoadContentStoryOrComic(chapterID);

                        saveContentStory();
                    }
                }, throwable -> {
//                    Toast.makeText(this, "[CANT GET Chapter Name]" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.d("CANTGETCHAPTERNAME", throwable.getMessage());
                }));
    }

    private void updateTitle() {
        txt_title_readStory.setText(chapter.get(i).getName());

    }

    private void eventLoadContentStoryOrComic(int id) {
        disposable.add(mservice.getContentStoryByChapterID(id)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(linkModel -> {
                    if (linkModel.isSuccess()) {


                        linkList = linkModel.getResult();
//                        Log.d("comic",Common.currentIsComic + "");
                        if (!Common.currentIsComic){
                            loadStory();
                            saveContentStory();
                        }
                        else {
                            Log.d("Comic",linkList.get(positionLinkList).getLink() + "");
                            Log.d("listsize",linkList.size() + "");
                            loadComic(linkList);
                            saveContentStory();
                        }
                    }
                }, throwable -> {
                    Toast.makeText(this, "[CANT GET CONTENT]" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                    Log.d("CANTGETCONTENT", throwable.getMessage());
                }));
    }

    private void loadComic(List<Link> linkList) {

        layout_story.setVisibility(View.GONE);
        layout_comic.setVisibility(View.VISIBLE);

        recycler_list_comic.setHasFixedSize(true);
        LinearLayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recycler_list_comic.setLayoutManager(manager);

        adapterListComic = new AdapterListComic(this,linkList);
        recycler_list_comic.setAdapter(adapterListComic);




    }

    private void loadStory() {
        txt_content_readStory.setText(linkList.get(positionLinkList).getLink());
    }


    private void saveContentStory(){
        Content content = new Content();
        content.setTitle(chapter.get(i).getName());
        content.setID(Common.currentIDStory);
        content.setLinkChap(linkList.get(positionLinkList).getLink());
        content.setComic(Common.currentIsComic);

        disposable.add(iStoryDataSource.insertContentOrReplace(content)
        .observeOn(AndroidSchedulers.mainThread())
        .subscribeOn(Schedulers.io())
        .subscribe(() -> {

        },throwable -> {
            Log.d("CANTSAVECONTENT",throwable.getMessage());
            Toast.makeText(this, "[CAN NOT SAVE CONTENT] "+throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }));

    }


    private void checkPermissionInternet() {
        ConnectivityManager manager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo celluar = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi.isConnected()){
//            Toast.makeText(getActivity(), "Wifi", Toast.LENGTH_LONG).show();
            eventLoadTitleStory();
            eventBtnNextChap();
            eventBtnPreviousChap();
        }
        else if (celluar.isConnected()){
            eventLoadTitleStory();
            eventBtnNextChap();
            eventBtnPreviousChap();
//            Toast.makeText(getActivity(), "Data Celluar", Toast.LENGTH_LONG).show();
        }else {
//            Toast.makeText(getActivity(), "No NetWork", Toast.LENGTH_LONG).show();
//            loaddContentFromDB();
            loaddContentFromDB();

        }

    }

    private void loaddContentFromDB() {
        disposable.add(iStoryDataSource.getContentList()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(contentList -> {
            displayContent(contentList);

        },throwable -> {
            Log.d("CANTLOADCONTENT",throwable.getMessage());
            Toast.makeText(this, "[CAN NOT LOAD CONTENT] "+throwable.getMessage(), Toast.LENGTH_SHORT).show();
        }));
    }

    private void displayContent(List<Content> contentList) {
        IDFromDB = contentList.get(0).getID();
        Log.d("id",IDFromDB + "");
        if (Common.currentIDStoryCheck != IDFromDB ){
            AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setMessage("Vui kết nối internet để có thể xem nội dung");
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            btn_back.setVisibility(View.GONE);
            btn_next.setVisibility(View.GONE);
            builder.show();

        }
        else
        if(!contentList.get(0).isComic){
            txt_title_readStory.setText(contentList.get(0).getTitle());
            txt_content_readStory.setText(contentList.get(0).getLinkChap());
        }else {
            layout_story.setVisibility(View.GONE);
            layout_comic.setVisibility(View.VISIBLE);

            recycler_list_comic.setHasFixedSize(true);
            LinearLayoutManager manager = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
            recycler_list_comic.setLayoutManager(manager);

            adapterListComic = new AdapterListComic(this,contentList);
            recycler_list_comic.setAdapter(adapterListComic);

        }


    }

    @Override
    protected void onDestroy() {
        disposable.clear();
        super.onDestroy();
    }

}
