package z79.trainingcourse.appcomicreadertwo.utils;

import android.content.Context;
import android.content.SharedPreferences;

public class SaveKey {

    private Context context;

    public SaveKey(Context context) {
        this.context = context;
    }

    public void saveKey(String key,String value){
        SharedPreferences sharedPreferences = context.getSharedPreferences(Common.KEY_USER,Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putString(key,value);
        editor.apply();
    }

    public String readKey(String key){
        SharedPreferences sharedPreferences = context.getSharedPreferences(Common.KEY_USER,Context.MODE_PRIVATE);
        if (sharedPreferences != null){
            return sharedPreferences.getString(key,"");
        }
        return null;
    }
}
