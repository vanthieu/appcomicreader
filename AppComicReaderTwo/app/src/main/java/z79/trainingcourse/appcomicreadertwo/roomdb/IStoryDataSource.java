package z79.trainingcourse.appcomicreadertwo.roomdb;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import z79.trainingcourse.appcomicreadertwo.model.Story;

public interface IStoryDataSource {

    Completable insertStoryOrReplace(List<Story> storyList);

    Flowable<List<Story>> getAllListStory();

    Completable insertFavoriteOrReplace(Favorite favoriteList);

    Flowable<List<Favorite>> getFavorite();

    Completable insertContentOrReplace(Content contentList);

    Flowable<List<Content>> getContentList();

    void insertFav(Favorite...favorites);

    void delete(Favorite... favorites);
}
