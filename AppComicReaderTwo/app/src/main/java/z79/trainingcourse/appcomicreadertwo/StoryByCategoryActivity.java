package z79.trainingcourse.appcomicreadertwo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.os.Bundle;
import android.util.Log;
import android.widget.TextView;
import android.widget.Toast;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import z79.trainingcourse.appcomicreadertwo.adapter.AdapterListStoryByCategoryId;
import z79.trainingcourse.appcomicreadertwo.model.Story;
import z79.trainingcourse.appcomicreadertwo.model.event.EventLoadStoryByCategoryId;
import z79.trainingcourse.appcomicreadertwo.retrofit.APIComic;
import z79.trainingcourse.appcomicreadertwo.retrofit.RetrofitClient;
import z79.trainingcourse.appcomicreadertwo.utils.Common;

public class StoryByCategoryActivity extends AppCompatActivity {


    @BindView(R.id.recycler_list_storyByCategoryID)
    RecyclerView recycler_list_storyByCategoryID;
    @BindView(R.id.txt_title_actionBar)
    TextView txt_title_actionBar;


    CompositeDisposable disposable;
    APIComic mservice;

    List<Story> listStoryByCategoryID;
    AdapterListStoryByCategoryId adapterListStoryByCategoryId;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_story_by_category);
        ButterKnife.bind(this);

        mservice = RetrofitClient.getRetrofit(Common.API_COMIC_ENPOINT).create(APIComic.class);
        disposable = new CompositeDisposable();


        init();

    }

    private void init() {

    }

    @Subscribe(sticky = true,threadMode = ThreadMode.MAIN)
    public void eventLoadStoryByCategoryId(EventLoadStoryByCategoryId event){
        if (event.isSuccess()){
//            Log.d("namecate",event.getCategories().getName() + "");
            disposable.add(mservice.getStoryByCateGoryID(event.getCategories().getID())
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(storyModel -> {
                        listStoryByCategoryID = new ArrayList<>();
                        if (storyModel.isSuccess()){
                            listStoryByCategoryID = storyModel.getResult();
//                            Log.d("listStory",listStoryByCategoryID.size() + "");
                            Log.d("size",listStoryByCategoryID.size() + "");
                            displayListStoryByCategoryId(listStoryByCategoryID);


                        }else {
                            Toast.makeText(this, "Thư viện đang cập nhật", Toast.LENGTH_SHORT).show();
                        }
                    },throwable -> {
                        Toast.makeText(this, "CAN NOT GET STORY BY CID" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("CANTGETSTORYBYCID",throwable.getMessage()+"");
                    }));
        }
    }

    private void displayListStoryByCategoryId(List<Story> listStoryByCategoryID) {
        configRecyclerView();
        adapterListStoryByCategoryId = new AdapterListStoryByCategoryId(this,listStoryByCategoryID);
        recycler_list_storyByCategoryID.setAdapter(adapterListStoryByCategoryId);
    }

    private void configRecyclerView() {
        LinearLayoutManager manager  = new LinearLayoutManager(this,LinearLayoutManager.VERTICAL,false);
        recycler_list_storyByCategoryID.setLayoutManager(manager);
        recycler_list_storyByCategoryID.setHasFixedSize(true);
    }


    @Override
    protected void onStart() {
        if (!EventBus.getDefault().isRegistered(this)){
            EventBus.getDefault().register(this);
        }
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        super.onDestroy();
    }
}
