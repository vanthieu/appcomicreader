package z79.trainingcourse.appcomicreadertwo.roomdb;

import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import z79.trainingcourse.appcomicreadertwo.model.Story;

@Dao
public interface IStoryDAO {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insertStoryOrReplace(List<Story> storyList);

    @Query("SELECT * FROM Story ORDER BY ID DESC")
    Flowable<List<Story>> getAllListStory();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insertFavoriteOrReplace(Favorite favorite);

    @Query("SELECT * FROM Favorite")
    Flowable<List<Favorite>> getFavorite();

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    Completable insertContentOrReplace(Content contentList);

    @Query("SELECT * FROM Content")
    Flowable<List<Content>> getContentList();

    @Insert
    void insertFav(Favorite...favorites);

    @Delete
    void delete(Favorite... favorites);




}
