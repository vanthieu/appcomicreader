package z79.trainingcourse.appcomicreadertwo.fragment;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;

import com.bumptech.glide.Glide;
import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.internal.CallbackManagerImpl;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInClient;
import com.google.android.gms.auth.api.signin.GoogleSignInOptions;
import com.google.android.gms.common.SignInButton;
import com.google.android.gms.common.api.ApiException;
import com.google.android.gms.tasks.Task;
import com.google.android.material.textfield.TextInputEditText;

import org.json.JSONException;
import org.json.JSONObject;

import butterknife.BindView;
import butterknife.ButterKnife;
import de.hdodenhof.circleimageview.CircleImageView;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import z79.trainingcourse.appcomicreadertwo.R;
import z79.trainingcourse.appcomicreadertwo.RegisterInforActivity;
import z79.trainingcourse.appcomicreadertwo.model.Users;
import z79.trainingcourse.appcomicreadertwo.retrofit.APIComic;
import z79.trainingcourse.appcomicreadertwo.retrofit.RetrofitClient;
import z79.trainingcourse.appcomicreadertwo.utils.Common;
import z79.trainingcourse.appcomicreadertwo.utils.SaveKey;

public class FragmentAccount extends Fragment {

    private static final int RC_SIGN_IN = 9999;

    @BindView(R.id.txt_register_account)
    TextView txt_register_account;
    @BindView(R.id.btn_signIn_default)
    Button btn_signIn_default;

    @BindView(R.id.edt_user_email_account)
    TextInputEditText edt_user_email_account;
    @BindView(R.id.edt_user_password_account)
    TextInputEditText edt_user_password_account;
    @BindView(R.id.layout_sign_out)
    RelativeLayout layout_sign_out;
    @BindView(R.id.layout_account_host)
    LinearLayout layout_account_host;
    @BindView(R.id.txt_name_user_signout)
    TextView txt_name_user_signout;
    @BindView(R.id.btn_facebook_signin)
    LoginButton btn_facebook_signin;
    @BindView(R.id.circleimg_avatar_account_signOut)
    CircleImageView circleimg_avatar_account_signOut;
    @BindView(R.id.btn_signout)
    Button btn_signout;
    @BindView(R.id.btn_signIn_google)
    SignInButton btn_signIn_google;


    APIComic mservice;
    CompositeDisposable disposable;
    Users users;

    SaveKey saveKey;

    View fragmentAccount;

    CallbackManager callbackManager;
    GoogleSignInClient googleSignInClient;
    GoogleSignInAccount account;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        fragmentAccount = inflater.inflate(R.layout.fragment_account, container, false);

        ButterKnife.bind(this, fragmentAccount);
        mservice = RetrofitClient.getRetrofit(Common.API_COMIC_ENPOINT).create(APIComic.class);
        disposable = new CompositeDisposable();
        users = new Users();
        saveKey = new SaveKey(getContext());

        callbackManager = CallbackManager.Factory.create();
        btn_facebook_signin.setFragment(this);

        /*google Sign */
        GoogleSignInOptions options = new GoogleSignInOptions.Builder(GoogleSignInOptions.DEFAULT_SIGN_IN)
                .requestEmail()
                .build();

        googleSignInClient = GoogleSignIn.getClient(getContext(), options);

        /*google Sign */


//        loginButton.setReadPermissions(Arrays.asList(
//                "public_profile", "email", "user_birthday", "user_friends"));

        /*temporary pause */

        if (saveKey.readKey(Common.USER_ID) != "" ) {
            txt_name_user_signout.setText(saveKey.readKey(Common.USER_ID));
            circleimg_avatar_account_signOut.setVisibility(View.GONE);
            layout_sign_out.setVisibility(View.VISIBLE);
            layout_account_host.setVisibility(View.GONE);

        }
        if (AccessToken.getCurrentAccessToken() != null) {

            layout_sign_out.setVisibility(View.VISIBLE);
            layout_account_host.setVisibility(View.GONE);
            circleimg_avatar_account_signOut.setVisibility(View.VISIBLE);
            getProfileFacebook(AccessToken.getCurrentAccessToken());
        }

        account = GoogleSignIn.getLastSignedInAccount(getActivity());
        if (account != null){
            updateUI(account);
        }

        checkPermissionInternet();
//        init();
        return fragmentAccount;
    }

    private void init() {

        eventRegisterAccount();
        eventButtonSignInDefault();
        eventButtonSignInFacebook();
        eventButtonSignInGoogle();
        eventSingOut();
    }

    private void checkPermissionInternet() {
        ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo celluar = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi.isConnected()){
//            Toast.makeText(getActivity(), "Wifi", Toast.LENGTH_LONG).show();
            init();
        }
        else if (celluar.isConnected()){
            init();
//            Toast.makeText(getActivity(), "Data Celluar", Toast.LENGTH_LONG).show();
        }else {
//            Toast.makeText(getActivity(), "No NetWork", Toast.LENGTH_LONG).show();
            AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
            builder.setMessage("Vui kết nối internet để đăng nhập");
            builder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });
            builder.show();

        }

    }

    private void eventButtonSignInGoogle() {
        btn_signIn_google.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent signInIntent = googleSignInClient.getSignInIntent();
                startActivityForResult(signInIntent, RC_SIGN_IN);

            }
        });
    }


    private void eventButtonSignInFacebook() {
        btn_facebook_signin.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                if (loginResult.getAccessToken() != null) {
                    getProfileFacebook(loginResult.getAccessToken());
                    Toast.makeText(getContext(), "Login Success", Toast.LENGTH_SHORT).show();

                    layout_account_host.setVisibility(View.GONE);
                    layout_sign_out.setVisibility(View.VISIBLE);

                }
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {

            }
        });
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == CallbackManagerImpl.RequestCodeOffset.Login.toRequestCode()){
            callbackManager.onActivityResult(requestCode,resultCode,data);
        }
        else if (requestCode == RC_SIGN_IN){
            Task<GoogleSignInAccount> accountTask = GoogleSignIn.getSignedInAccountFromIntent(data);
            handleSignInResult(accountTask);
        }

    }

    private void handleSignInResult(Task<GoogleSignInAccount> accountTask) {
        try {
            account = accountTask.getResult(ApiException.class);
            updateUI(account);
        } catch (ApiException e) {
            e.printStackTrace();
            Log.w("GoogleSignInError", "signInResult:failed code=" + e.getStatusCode());
            Log.d("GoogleSignInError", e.getStatusCode() + "" + e.getMessage());
            Toast.makeText(getContext(), "Failed", Toast.LENGTH_LONG).show();
        }


    }

    private void updateUI(GoogleSignInAccount account) {
        if (account != null) {
            saveKey.saveKey(Common.USER_ID,account.getDisplayName());
            Toast.makeText(getContext(), "You already In", Toast.LENGTH_SHORT).show();
            layout_account_host.setVisibility(View.GONE);
            layout_sign_out.setVisibility(View.VISIBLE);
            txt_name_user_signout.setText(account.getDisplayName());
            circleimg_avatar_account_signOut.setVisibility(View.GONE);
        }
    }


    private void getProfileFacebook(AccessToken accessToken) {

        GraphRequest request = GraphRequest.newMeRequest(accessToken, new GraphRequest.GraphJSONObjectCallback() {
            @Override
            public void onCompleted(JSONObject object, GraphResponse response) {
                try {
                    String email = object.getString("email");
                    String last_name = object.getString("last_name");
                    String first_name = object.getString("first_name");
                    String id = object.getString("id");
                    String url_img = "https://graph.facebook.com/" + id + "/picture?type=large";

                    Log.v("LoginActivityResponse", response.toString());

                    txt_name_user_signout.setText(first_name + " " + last_name);
                    Glide.with(getContext()).load(url_img).into(circleimg_avatar_account_signOut);

                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        Bundle bundle = new Bundle();
        bundle.putString("fields", "last_name,first_name,email,id");
        request.setParameters(bundle);
        request.executeAsync();
    }


    private void eventSingOut() {
        btn_signout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                saveKey.saveKey(Common.USER_ID, "");
//                txt_name_user_signout.setText("");
//                layout_sign_out.setVisibility(View.GONE);
//                layout_account_host.setVisibility(View.VISIBLE);
//
//
//                if (AccessToken.getCurrentAccessToken()!=null){
//                    LoginManager.getInstance().logOut();
//                }else
//                    if (account != null){
//
//                        googleSignInClient.signOut();
//                        googleSignInClient.revokeAccess();
//                    }

                AlertDialog.Builder shows = new AlertDialog.Builder(getActivity());
                    shows.setTitle("Sign Out");
                    shows.setMessage("You Want To Sign Out ?");
                    shows.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    shows.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            saveKey.saveKey(Common.USER_ID, "");
                            txt_name_user_signout.setText("");
                            layout_sign_out.setVisibility(View.GONE);
                            layout_account_host.setVisibility(View.VISIBLE);


                            if (AccessToken.getCurrentAccessToken()!=null){
                                LoginManager.getInstance().logOut();
                            }else
                            if (account != null){

                                googleSignInClient.signOut();
                                googleSignInClient.revokeAccess();
                            }

                        }
                    });

                    shows.show();
            }
        });
    }

    private void eventButtonSignInDefault() {
        btn_signIn_default.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(edt_user_email_account.getText().toString()) || TextUtils.isEmpty(edt_user_password_account.getText().toString())) {
                    Toast.makeText(getContext(), "Please Fill Text", Toast.LENGTH_SHORT).show();
                } else {
                    login(edt_user_email_account.getText().toString(), edt_user_password_account.getText().toString());
                }
            }
        });
    }

    private void login(String email, String password) {
        disposable.add(mservice.loginUser(email, password)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(loginUserModel -> {
                    if (loginUserModel.isSuccess()) {

                        users = loginUserModel.getUsers();
                        layout_account_host.setVisibility(View.GONE);
                        layout_sign_out.setVisibility(View.VISIBLE);
                        circleimg_avatar_account_signOut.setVisibility(View.GONE);
                        txt_name_user_signout.setText(users.getUserName());


                        Toast.makeText(getContext(), "Login Success", Toast.LENGTH_SHORT).show();

                        Log.d("user", users.getUserName() + "");

                        //save key
                        saveKey.saveKey(Common.USER_ID, users.getUserName());

                    } else {
                        Toast.makeText(getContext(), "Wrong Password Or Email", Toast.LENGTH_SHORT).show();

                    }
                }, throwable -> {
                    Log.d("CANTLOGIN", throwable.getMessage());
                    Toast.makeText(getContext(), "[CAN NOT LOGIN]" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }));
    }


    private void eventRegisterAccount() {
        txt_register_account.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displayFormRegister();
            }
        });
    }

    private void displayFormRegister() {
        startActivity(new Intent(getActivity(), RegisterInforActivity.class));
    }


    @Override
    public void onDestroy() {
        disposable.clear();
        super.onDestroy();
    }

    @Override
    public void onResume() {
        if (AccessToken.getCurrentAccessToken() != null) {

            layout_sign_out.setVisibility(View.VISIBLE);
            layout_account_host.setVisibility(View.GONE);
            circleimg_avatar_account_signOut.setVisibility(View.VISIBLE);
            getProfileFacebook(AccessToken.getCurrentAccessToken());
        }

        account = GoogleSignIn.getLastSignedInAccount(getActivity());
        if (account != null){
            updateUI(account);
        }
        super.onResume();
    }
}
