package z79.trainingcourse.appcomicreadertwo.roomdb;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Favorite")
public class Favorite {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID")
    public int ID;
    @ColumnInfo(name = "Name")
    public String Name;
    @ColumnInfo(name = "Image")
    public String Image;
    @ColumnInfo(name = "NumOfChap")
    public int NumOfChap;

    public Favorite() {
    }

    public Favorite(int ID, String name, String image, int numOfChap) {
        this.ID = ID;
        Name = name;
        Image = image;
        NumOfChap = numOfChap;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public int getNumOfChap() {
        return NumOfChap;
    }

    public void setNumOfChap(int numOfChap) {
        NumOfChap = numOfChap;
    }
}
