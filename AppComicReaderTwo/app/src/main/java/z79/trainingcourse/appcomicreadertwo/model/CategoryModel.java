package z79.trainingcourse.appcomicreadertwo.model;

import java.util.List;

public class CategoryModel {

    private boolean success;
    private String message;
    private List<Categories> result;

    public CategoryModel() {
    }

    public CategoryModel(boolean success, String message, List<Categories> result) {
        this.success = success;
        this.message = message;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Categories> getResult() {
        return result;
    }

    public void setResult(List<Categories> result) {
        this.result = result;
    }
}
