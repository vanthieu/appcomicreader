package z79.trainingcourse.appcomicreadertwo.roomdb;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "Content")
public class Content {

    @PrimaryKey()
    @NonNull
    @ColumnInfo(name = "ID")
    public int ID;
    @ColumnInfo(name = "Title")
    public String Title;
    @ColumnInfo(name = "LinkChap")
    public String LinkChap;
    @ColumnInfo(name = "isComic")
    public boolean isComic;

    public Content() {
    }

    public Content(int ID, String title, String linkChap, boolean isComic) {
        this.ID = ID;
        Title = title;
        LinkChap = linkChap;
        this.isComic = isComic;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getLinkChap() {
        return LinkChap;
    }

    public void setLinkChap(String linkChap) {
        LinkChap = linkChap;
    }

    public boolean isComic() {
        return isComic;
    }

    public void setComic(boolean comic) {
        isComic = comic;
    }
}
