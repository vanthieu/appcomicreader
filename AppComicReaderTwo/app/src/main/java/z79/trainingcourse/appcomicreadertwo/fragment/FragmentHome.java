package z79.trainingcourse.appcomicreadertwo.fragment;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;
import java.util.ArrayList;
import java.util.List;
import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import z79.trainingcourse.appcomicreadertwo.R;
import z79.trainingcourse.appcomicreadertwo.adapter.AdapterListFragmentHome;
import z79.trainingcourse.appcomicreadertwo.model.Story;
import z79.trainingcourse.appcomicreadertwo.retrofit.APIComic;
import z79.trainingcourse.appcomicreadertwo.retrofit.RetrofitClient;
import z79.trainingcourse.appcomicreadertwo.roomdb.IStoryDataSource;
import z79.trainingcourse.appcomicreadertwo.roomdb.LocalStoryDataSource;
import z79.trainingcourse.appcomicreadertwo.roomdb.StoryDatabase;
import z79.trainingcourse.appcomicreadertwo.utils.Common;

public class FragmentHome extends Fragment {


    @BindView(R.id.recycler_list_story)
    public RecyclerView recycler_list_story;
    AdapterListFragmentHome adapterListFragmentHome, adapterSearch;

    IStoryDataSource iStoryDataSource;

    APIComic mservice;
    CompositeDisposable disposable;

    LinearLayoutManager manager;

    List<Story> listStory, searchList,listFromAPI;
    int maxData = 0;


    LinearLayoutManager linearLayoutManager;


    boolean isLoading = false;

    int permission;

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mservice = RetrofitClient.getRetrofit(Common.API_COMIC_ENPOINT).create(APIComic.class);
        disposable = new CompositeDisposable();
        iStoryDataSource = new LocalStoryDataSource(StoryDatabase.getInstance(getContext()).iStoryDAO());

        listStory = new ArrayList<>();
        searchList = new ArrayList<>();


        View view = inflater.inflate(R.layout.fragment_home, container, false);
        ButterKnife.bind(this, view);



        init();
        checkPermissionReadStore();
        checkPermissionInternet();
        return view;
    }

    private void checkPermissionInternet() {
        ConnectivityManager manager = (ConnectivityManager) getActivity().getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo wifi = manager.getNetworkInfo(ConnectivityManager.TYPE_WIFI);
        NetworkInfo celluar = manager.getNetworkInfo(ConnectivityManager.TYPE_MOBILE);

        if (wifi.isConnected()){
//            Toast.makeText(getActivity(), "Wifi", Toast.LENGTH_LONG).show();
            loadAllStory();
        }
        else if (celluar.isConnected()){
            loadAllStory();
//            Toast.makeText(getActivity(), "Data Celluar", Toast.LENGTH_LONG).show();
        }else {
//            Toast.makeText(getActivity(), "No NetWork", Toast.LENGTH_LONG).show();
            loaddAllStoryFromDB();
        }

    }

    private void loaddAllStoryFromDB() {
        disposable.add(iStoryDataSource.getAllListStory()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(storyList -> {
                    displayStoryFromDB(storyList);
                },throwable -> {
                    Toast.makeText(getActivity(), "[CAN NOT GET STORY]", Toast.LENGTH_SHORT).show();
                    Log.d("CANTGETSTORY",throwable.getMessage());
                }));
    }

    private void displayStoryFromDB(List<Story> storyList) {
        if (storyList.size() > 0){
            listStory.clear();
            listStory.addAll(storyList);
            adapterListFragmentHome = new AdapterListFragmentHome(getContext(), listStory);
            recycler_list_story.setAdapter(adapterListFragmentHome);

        } else {
            Toast.makeText(getContext(), "Story Empty From Phone", Toast.LENGTH_SHORT).show();
        }
    }


    public void checkPermissionReadStore() {
        permission = ContextCompat.checkSelfPermission(getActivity(),Manifest.permission.READ_EXTERNAL_STORAGE);
        if (permission != PackageManager.PERMISSION_GRANTED){
            ActivityCompat.requestPermissions(getActivity(),new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE,Manifest.permission.READ_EXTERNAL_STORAGE},
                    Common.REQUEST_PERMISSION);
        }
    }


    public void saveData() {
        List<Story> listSave = new ArrayList<>();

        listSave.addAll(listStory);

        disposable.add(iStoryDataSource.insertStoryOrReplace(listSave)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                        Toast.makeText(getContext(), "SAVE DATA SUCCESS", Toast.LENGTH_SHORT).show();

                }, throwable -> {
                    Toast.makeText(getContext(), "[CAN NOT SAVEDATA]", Toast.LENGTH_SHORT).show();
                    Log.d("CANTSAVEDATA", throwable.getMessage().toString());
                }));
    }


    private void init() {
        configRecyclerListStory();

//        loadListStoryLimitFromAPITest(0,10);
//        loadAllStory();
        configRecyclerSrollView();
    }

    private void configRecyclerSrollView() {

        linearLayoutManager = (LinearLayoutManager) recycler_list_story.getLayoutManager();
        recycler_list_story.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);

                if (!isLoading) {
                    if (recycler_list_story.canScrollVertically(0) == false) {

                        loadMore();
                        isLoading = true;
                    }
                }

            }
        });
    }

    public void backSearch() {
        recycler_list_story.setAdapter(adapterListFragmentHome);
    }


    public void searchText(Editable editable) {
        if (editable.toString().equals("")) {
            searchList.clear();
            adapterSearch.notifyDataSetChanged();


        } else {

            disposable.add(mservice.searchStory(String.valueOf(editable))
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe(searchModel -> {
                        if (searchModel.isSuccess()) {

                            searchList = searchModel.getResult();
                            adapterSearch = new AdapterListFragmentHome(getContext(), searchList);
                            recycler_list_story.setAdapter(adapterSearch);

                        }
                    }, throwable -> {
                        Toast.makeText(getContext(), "[CANT GET SEARCH]" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                        Log.d("CANTGETSEARCH", throwable.getMessage());
                    }));

        }
    }

    public void loadAllStory() {
        disposable.add(mservice.getAllStory()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(allStoryModel -> {
                    if (allStoryModel.isSuccess()) {

                        maxData = allStoryModel.getResult().get(0).getMaxRowNum();
                        loadStoryLimit(0, 10);


                    } else {
                        Toast.makeText(getContext(), "[ALL STORY UNSUCCESS]" + allStoryModel.getMessage(), Toast.LENGTH_LONG).show();
                        Log.d("ALLSTORYUNSUCCESS", allStoryModel.getMessage());
                    }
                }, throwable -> {
                    Toast.makeText(getContext(), "[CANT GET ALL STORY]" + throwable.getMessage(), Toast.LENGTH_LONG).show();
                    Log.d("CANTGETALLSTORY", throwable.getMessage());
                }));
    }

    private void loadStoryLimit(int from, int to) {
        disposable.add(mservice.getStoryLimit(from, to)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(storyModel -> {
                    if (storyModel.isSuccess()) {
                        if (adapterListFragmentHome == null) {

                            //add data
//                            listStory = new ArrayList<>();
                            listFromAPI = storyModel.getResult();
                            listStory.clear();
                            listStory.addAll(listFromAPI);

                            //set dapter
                            adapterListFragmentHome = new AdapterListFragmentHome(getContext(), listStory);
                            recycler_list_story.setAdapter(adapterListFragmentHome);

//                            checkPermissionReadStore();
                            if (permission == PackageManager.PERMISSION_GRANTED){
                                saveData();
                            }

                        } else {
                            //set listen loadmore
                            //check and turn of progressbar
                            isLoading = true;
                            listStory.remove(listStory.size() - 1);
                            listStory = storyModel.getResult();

                            if (permission == PackageManager.PERMISSION_GRANTED){
                                saveData();
                            }
                            adapterListFragmentHome.addItems(listStory);

                        }
                    } else {
                        Toast.makeText(getContext(), "[ALL STORY UNSUCCESS]" + storyModel.getMessage(), Toast.LENGTH_LONG).show();
                        Log.d("STORYLIMITUNSUCCESS", storyModel.getMessage());
                    }
                }, throwable -> {
                    Toast.makeText(getContext(), "[CANT GET STORY LIMIT ]" + throwable.getMessage(), Toast.LENGTH_LONG).show();
                    Log.d("CANTGETSTORYLIMIT", throwable.getMessage());
                }));
    }


    private void configRecyclerListStory() {
        manager = new LinearLayoutManager(getContext(), LinearLayoutManager.VERTICAL, false);
        recycler_list_story.setLayoutManager(manager);
        recycler_list_story.setHasFixedSize(true);

    }


    /*-------------------------------*/
    /*Only Load Story Limit*/
//    private void loadListStoryLimitFromAPITest(int from, int to) {
//
//        disposable.add(mservice.getStoryLimit(from, to)
//                .subscribeOn(Schedulers.io())
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe(storyModel -> {
//
//                    if (storyModel.isSuccess()) {
//
//                        displayListStory(storyModel.getResult());
//
//
//                    } else {
//                        Toast.makeText(getContext(), "[UNSUCCESS]" + storyModel.getMessage(), Toast.LENGTH_SHORT).show();
//                        Log.d("UNSUCCESS", storyModel.getMessage());
//                    }
//
//                }, throwable -> {
//                    Toast.makeText(getContext(), "[CAN GET LIST STORY]" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
//                    Log.d("network", throwable.getMessage());
//                }));
//    }
//
//    private void displayListStory(List<Story> result) {
//
//        adapterListFragmentHome = new AdapterListFragmentHome(getContext(), result);
//        recycler_list_story.setAdapter(adapterListFragmentHome);
//    }

    /*Only Load Story Limit*/
    /*-------------------------------*/

    @Override
    public void onStop() {
        disposable.clear();
        super.onStop();

    }

    private void loadMore() {
        Log.e("TODO", "TOOD");

        if (listStory.size() < maxData) {
//            //add list null to anounce progressbar turn on
            listStory.add(null);
//
            adapterListFragmentHome.notifyDataSetChanged();

            //call load list story
            loadStoryLimit(adapterListFragmentHome.getItemCount() - 1, adapterListFragmentHome.getItemCount() + 10);
//            adapterListFragmentHome.notifyDataSetChanged();
            setLoaded();

        } else if (listStory.size() > maxData) {
            Toast.makeText(getContext(), "Hết Truyện", Toast.LENGTH_SHORT).show();
        }
    }



    public void setLoaded() {
        isLoading = false;
    }


}

