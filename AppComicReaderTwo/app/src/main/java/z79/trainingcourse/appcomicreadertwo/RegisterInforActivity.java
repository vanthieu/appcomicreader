package z79.trainingcourse.appcomicreadertwo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;
import android.text.TextUtils;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.google.android.material.textfield.TextInputEditText;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import z79.trainingcourse.appcomicreadertwo.fragment.FragmentAccount;
import z79.trainingcourse.appcomicreadertwo.retrofit.APIComic;
import z79.trainingcourse.appcomicreadertwo.retrofit.RetrofitClient;
import z79.trainingcourse.appcomicreadertwo.utils.Common;

public class RegisterInforActivity extends AppCompatActivity {

    @BindView(R.id.edt_name_register)
    TextInputEditText edt_name_register;
    @BindView(R.id.edt_email_register)
    TextInputEditText edt_email_register;
    @BindView(R.id.edt_password_register)
    TextInputEditText edt_password_register;
    @BindView(R.id.btn_registerInfo)
    Button btn_registerInfo;

    APIComic mservice;
    CompositeDisposable disposable;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_infor);
        ButterKnife.bind(this);
        mservice = RetrofitClient.getRetrofit(Common.API_COMIC_ENPOINT).create(APIComic.class);
        disposable = new CompositeDisposable();

        init();

    }

    private void init() {
        eventRegister();
    }

    private void eventRegister() {
        btn_registerInfo.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TextUtils.isEmpty(edt_email_register.getText().toString()) || TextUtils.isEmpty(edt_name_register.getText().toString())
                        || TextUtils.isEmpty(edt_password_register.getText().toString())) {
                    Toast.makeText(RegisterInforActivity.this, "Please fill empty", Toast.LENGTH_SHORT).show();
                } else if (edt_password_register.getText().toString().length() < 8 && !Common.isValidPassword(edt_password_register.getText().toString())) {
                    Toast.makeText(RegisterInforActivity.this, "Make strong password aleast 8 character with upper case", Toast.LENGTH_SHORT).show();
                } else {

                    registerInfor();


                }


            }
        });
    }

    private void registerInfor() {
        disposable.add(mservice.newUser(edt_name_register.getText().toString(),
                edt_email_register.getText().toString(),
                edt_password_register.getText().toString())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(userModel -> {
                    if (userModel.isSuccess()) {
                        Toast.makeText(this, "Register Success,Please Login", Toast.LENGTH_LONG).show();
                        //back to fragment account.
                        finish();
                    }
                }, throwable -> {
                    Log.d("Error", throwable.getMessage());
                    Toast.makeText(this, "[CAN NOT REGISTER]" + throwable.getMessage(), Toast.LENGTH_SHORT).show();
                }));
    }


    @Override
    protected void onDestroy() {
        disposable.clear();
        super.onDestroy();
    }
}
