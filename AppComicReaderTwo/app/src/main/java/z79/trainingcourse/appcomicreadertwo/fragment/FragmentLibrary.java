package z79.trainingcourse.appcomicreadertwo.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.snackbar.Snackbar;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import z79.trainingcourse.appcomicreadertwo.R;
import z79.trainingcourse.appcomicreadertwo.adapter.AdapterListFavorite;
import z79.trainingcourse.appcomicreadertwo.interfaces.IRecyclerItemTouchHelper;
import z79.trainingcourse.appcomicreadertwo.roomdb.Favorite;
import z79.trainingcourse.appcomicreadertwo.roomdb.IStoryDataSource;
import z79.trainingcourse.appcomicreadertwo.roomdb.LocalStoryDataSource;
import z79.trainingcourse.appcomicreadertwo.roomdb.StoryDatabase;
import z79.trainingcourse.appcomicreadertwo.utils.Common;
import z79.trainingcourse.appcomicreadertwo.utils.RecyclerItemTouchHelper;

public class FragmentLibrary extends Fragment implements IRecyclerItemTouchHelper {

    IStoryDataSource iStoryDataSource;
    CompositeDisposable disposable;

    @BindView(R.id.recycler_list_favorite)
    RecyclerView recycler_list_favorite;

    @BindView(R.id.fragment_library)
    LinearLayout fragment_library;

    AdapterListFavorite adapterListFavorite;

    List<Favorite> listFavLocal = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {


        disposable = new CompositeDisposable();
        View fragmentLibrary = inflater.inflate(R.layout.fragment_library, container, false);
        ButterKnife.bind(this, fragmentLibrary);
        iStoryDataSource = new LocalStoryDataSource(StoryDatabase.getInstance(getContext()).iStoryDAO());


        ItemTouchHelper.SimpleCallback simpleCallback = new RecyclerItemTouchHelper(0,ItemTouchHelper.LEFT,this);
        new ItemTouchHelper(simpleCallback).attachToRecyclerView(recycler_list_favorite);

        init();


        return fragmentLibrary;
    }

    private void init() {
        getFavFromDB();

    }


    private void getFavFromDB() {
        disposable.add(iStoryDataSource.getFavorite()
                .observeOn(AndroidSchedulers.mainThread())
                .subscribeOn(Schedulers.io())
                .subscribe(favoriteList -> {
                    displayFavorite(favoriteList);
                },throwable -> {
                    Log.d("CANTGETFAV",throwable.getMessage());
                }));
    }

    private void displayFavorite(List<Favorite> favoriteList) {
        listFavLocal = favoriteList;

        LinearLayoutManager manager = new LinearLayoutManager(getContext(),LinearLayoutManager.VERTICAL,false);
        recycler_list_favorite.setLayoutManager(manager);
        recycler_list_favorite.setHasFixedSize(true);


        adapterListFavorite = new AdapterListFavorite(getContext(),favoriteList);
        recycler_list_favorite.setAdapter(adapterListFavorite);
    }

    @Override
    public void onResume() {
        getFavFromDB();
        super.onResume();
    }

    @Override
    public void onDestroy() {
        disposable.clear();
        super.onDestroy();
    }


    @Override
    public void onSwiped(RecyclerView.ViewHolder holder, int direction, int position) {
        if (holder instanceof AdapterListFavorite.MyViewHolder){
            String name = listFavLocal.get(holder.getAdapterPosition()).Name;

            final Favorite itemDelete = listFavLocal.get(holder.getAdapterPosition());

            final int deleteIndex = holder.getAdapterPosition();

            //delete Item
            adapterListFavorite.removeItem(deleteIndex);
            //delete in roomdb
            iStoryDataSource.delete(itemDelete);

            //show undo
            Snackbar snackbar = Snackbar.make(fragment_library,new StringBuilder(name).append("Xóa"),
                    Snackbar.LENGTH_LONG);
            snackbar.setAction("Hoàn tác", new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    adapterListFavorite.restoreItem(itemDelete,deleteIndex);
                   iStoryDataSource.insertFav(itemDelete);
                }
            });

            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();

        }
    }
}
