package z79.trainingcourse.appcomicreadertwo.utils;

import android.content.Context;
import android.graphics.Canvas;
import android.view.View;

import androidx.recyclerview.widget.ItemTouchHelper;
import androidx.recyclerview.widget.ListAdapter;
import androidx.recyclerview.widget.RecyclerView;

import z79.trainingcourse.appcomicreadertwo.adapter.AdapterListFavorite;
import z79.trainingcourse.appcomicreadertwo.interfaces.IRecyclerItemTouchHelper;



public class RecyclerItemTouchHelper extends ItemTouchHelper.SimpleCallback {

    IRecyclerItemTouchHelper listener;

    public RecyclerItemTouchHelper(int dragDirs, int swipeDirs, IRecyclerItemTouchHelper listener) {
        super(dragDirs, swipeDirs);
        this.listener = listener;
    }



    @Override
    public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
        return true;
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
        if (listener != null)
            listener.onSwiped(viewHolder, direction, viewHolder.getAdapterPosition());
    }

    @Override
    public void clearView(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder) {

            View foregroundView = ((AdapterListFavorite.MyViewHolder) viewHolder).view_foreground;
            getDefaultUIUtil().clearView(foregroundView);


    }

    @Override
    public int convertToAbsoluteDirection(int flags, int layoutDirection) {
        return super.convertToAbsoluteDirection(flags, layoutDirection);
    }

    @Override
    public void onSelectedChanged(RecyclerView.ViewHolder viewHolder, int actionState) {
        if (viewHolder != null) {
            if (viewHolder instanceof AdapterListFavorite.MyViewHolder) {
                View foregroundView = ((AdapterListFavorite.MyViewHolder) viewHolder).view_foreground;
                getDefaultUIUtil().clearView(foregroundView);
            }
        }
    }

    @Override
    public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        if (viewHolder instanceof AdapterListFavorite.MyViewHolder) {
            View foregroundView = ((AdapterListFavorite.MyViewHolder) viewHolder).view_foreground;
            getDefaultUIUtil().onDraw(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive);
        }
    }

    @Override
    public void onChildDrawOver(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
        if (viewHolder instanceof AdapterListFavorite.MyViewHolder) {
            View foregroundView = ((AdapterListFavorite.MyViewHolder) viewHolder).view_foreground;
            getDefaultUIUtil().onDrawOver(c, recyclerView, foregroundView, dX, dY, actionState, isCurrentlyActive);
        }

    }
}
