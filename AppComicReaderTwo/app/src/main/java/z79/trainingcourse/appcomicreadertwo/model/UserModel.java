package z79.trainingcourse.appcomicreadertwo.model;

import java.util.List;

public class UserModel {

    private boolean success;
    List<Users> result;

    public UserModel() {
    }

    public UserModel(boolean success, List<Users> result) {
        this.success = success;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public List<Users> getResult() {
        return result;
    }

    public void setResult(List<Users> result) {
        this.result = result;
    }
}
