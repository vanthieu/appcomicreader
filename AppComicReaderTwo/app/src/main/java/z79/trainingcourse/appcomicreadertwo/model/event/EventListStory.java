package z79.trainingcourse.appcomicreadertwo.model.event;

import java.util.List;

import z79.trainingcourse.appcomicreadertwo.model.Story;

public class EventListStory {

    private boolean success;
    private Story story;

    public EventListStory() {
    }

    public EventListStory(boolean success, Story story) {
        this.success = success;
        this.story = story;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Story getStory() {
        return story;
    }

    public void setStory(Story story) {
        this.story = story;
    }
}
