package z79.trainingcourse.appcomicreadertwo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;


import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import z79.trainingcourse.appcomicreadertwo.DetailStoryActivity;
import z79.trainingcourse.appcomicreadertwo.R;
import z79.trainingcourse.appcomicreadertwo.interfaces.IClickListener;
import z79.trainingcourse.appcomicreadertwo.model.Story;
import z79.trainingcourse.appcomicreadertwo.model.event.EventListStory;
import z79.trainingcourse.appcomicreadertwo.utils.Common;

public class AdapterListFragmentHome extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    Context context;
    List<Story> listStory;
    SimpleDateFormat simpleDateFormat;

    public void clearItems() {
        listStory.clear();
        notifyDataSetChanged();
    }
    public void addItems(List<Story> addItems) {
        int startIndex = listStory.size();
        listStory.addAll(addItems);

        notifyItemInserted(startIndex);
    }

    public AdapterListFragmentHome(Context context, List<Story> listStory) {
        this.context = context;
        this.listStory = listStory;
        simpleDateFormat = new SimpleDateFormat("YYYY/MM/dd");

    }



    @NonNull
    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        if (viewType == Common.VIEW_TYPE_ITEM) {
            View view = LayoutInflater.from(context).inflate(R.layout.custom_fragment_list_home, parent, false);
            return new MyViewHolder(view);
        } else {
            View view = LayoutInflater.from(context).inflate(R.layout.layout_loading, parent, false);
            return new LoadingViewHolder(view);
        }

    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (holder instanceof MyViewHolder) {

            MyViewHolder viewHolder = (MyViewHolder) holder;
            Glide.with(context).load(listStory.get(position).getImage()).into(viewHolder.circleimg_avatar_listStory);

            viewHolder.txt_name_listStory.setText(listStory.get(position).getName());

            String status = listStory.get(position).getStatus();
            StringBuilder statusStory = new StringBuilder();
            statusStory.append("Trạng Thái : ");
            if (status.equals("loading")) {
                statusStory.append("Đang Cập Nhật");
            } else {
                statusStory.append("Đủ Bộ");
            }
            viewHolder.txt_status_listStory.setText(statusStory);

            viewHolder.txt_author_listStory.setText(listStory.get(position).getAuthor());

            viewHolder.txt_day_update_listStory.setText(new StringBuilder("Ngày Cập Nhật : " + simpleDateFormat.format(listStory.get(position).getDate_Update())));

            viewHolder.setClickListener(new IClickListener() {
                @Override
                public void itemClickListener(View view, int position) {
//                    Toast.makeText(context, ""+ listStory.get(position).getNumOfChap(), Toast.LENGTH_SHORT).show();
                    Common.currentIDStory = listStory.get(position).getID();
                    Common.currentIsComic = listStory.get(position).isComic();
                    EventBus.getDefault().postSticky(new EventListStory(true,listStory.get(position)));
                    context.startActivity(new Intent(context, DetailStoryActivity.class));
                }
            });

        } else if (holder instanceof LoadingViewHolder) {
            LoadingViewHolder loadingViewHolder = (LoadingViewHolder) holder;
            loadingViewHolder.progressBar_loading.setIndeterminate(true);
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (listStory.get(position) == null) {
            return Common.VIEW_TYPE_LOADING;
        } else {
            return Common.VIEW_TYPE_ITEM;
        }
    }

    @Override
    public int getItemCount() {
//        return listStory.size();
        return listStory == null ? 0 : listStory.size();
    }


    public class LoadingViewHolder extends RecyclerView.ViewHolder {

        Unbinder unbinder;
        @BindView(R.id.progressBar_loading)
        ProgressBar progressBar_loading;

        public LoadingViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
        }
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Unbinder unbinder;

        @BindView(R.id.circleimg_avatar_listStory)
        CircleImageView circleimg_avatar_listStory;
        @BindView(R.id.txt_name_listStory)
        TextView txt_name_listStory;
        @BindView(R.id.txt_status_listStory)
        TextView txt_status_listStory;
        @BindView(R.id.txt_author_listStory)
        TextView txt_author_listStory;
        @BindView(R.id.txt_day_update_listStory)
        TextView txt_day_update_listStory;

        IClickListener clickListener;

        public void setClickListener(IClickListener clickListener) {
            this.clickListener = clickListener;
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.itemClickListener(view,getAdapterPosition());
        }
    }
}
