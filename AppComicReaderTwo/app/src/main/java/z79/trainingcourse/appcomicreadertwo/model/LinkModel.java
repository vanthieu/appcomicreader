package z79.trainingcourse.appcomicreadertwo.model;

import java.util.List;

public class LinkModel {
    private boolean success;
    private String message;
    private List<Link> result;

    public LinkModel() {
    }

    public LinkModel(boolean success, String message, List<Link> result) {
        this.success = success;
        this.message = message;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Link> getResult() {
        return result;
    }

    public void setResult(List<Link> result) {
        this.result = result;
    }
}
