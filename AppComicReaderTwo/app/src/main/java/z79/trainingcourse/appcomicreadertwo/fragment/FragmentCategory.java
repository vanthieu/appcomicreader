package z79.trainingcourse.appcomicreadertwo.fragment;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.fragment.app.Fragment;
import androidx.recyclerview.widget.DividerItemDecoration;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import z79.trainingcourse.appcomicreadertwo.R;
import z79.trainingcourse.appcomicreadertwo.adapter.AdapterListFragmentCategory;
import z79.trainingcourse.appcomicreadertwo.model.Categories;
import z79.trainingcourse.appcomicreadertwo.retrofit.APIComic;
import z79.trainingcourse.appcomicreadertwo.retrofit.RetrofitClient;
import z79.trainingcourse.appcomicreadertwo.utils.Common;
import z79.trainingcourse.appcomicreadertwo.utils.ItemDecoration;

public class FragmentCategory extends Fragment {

    @BindView(R.id.recycler_list_category)
    RecyclerView recycler_list_category;

    APIComic mservice;
    CompositeDisposable disposable;

    AdapterListFragmentCategory adapterListFragmentCategory;
    List<Categories> categoriesList;


    @Nullable
    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        mservice = RetrofitClient.getRetrofit(Common.API_COMIC_ENPOINT).create(APIComic.class);
        disposable = new CompositeDisposable();

        View fragmentCategory = inflater.inflate(R.layout.fragment_category, container, false);
        ButterKnife.bind(this, fragmentCategory);

        init();
        return fragmentCategory;
    }

    private void init() {
        eventDevideLayoutRecycler();
        eventGetCategoryFromAPI();
    }

    private void eventGetCategoryFromAPI() {
        disposable.add(mservice.getAllCategory()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .subscribe(categoryModel -> {
            if (categoryModel.isSuccess()){
                categoriesList = new ArrayList<>();
                categoriesList = categoryModel.getResult();

                displayCategory(categoriesList);
            }
        },throwable -> {
            Toast.makeText(getContext(), "Vui lòng kết nối internet", Toast.LENGTH_SHORT).show();
            Log.d("CANTGETCATEGORY",throwable.getMessage()+"");
        }));
    }

    private void displayCategory(List<Categories> categoriesList) {
        adapterListFragmentCategory = new AdapterListFragmentCategory(getContext(),categoriesList);
        recycler_list_category.setAdapter(adapterListFragmentCategory);
    }

    private void eventDevideLayoutRecycler() {
        GridLayoutManager manager = new GridLayoutManager(getContext(), 2);
        manager.setOrientation(RecyclerView.VERTICAL);
        manager.setSpanSizeLookup(new GridLayoutManager.SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                if (adapterListFragmentCategory != null) {
                    switch (adapterListFragmentCategory.getItemViewType(position)) {
                        case Common.DEFAULT_COLUMN:
                            return 1;
                        case Common.FULL_WITH_COLUMN:
                            return 2;
                        default:
                            return -1;
                    }
                } else
                    return -1;
            }
        });

        recycler_list_category.setHasFixedSize(true);
        recycler_list_category.addItemDecoration(new ItemDecoration(8));
        recycler_list_category.setLayoutManager(manager);




    }

    @Override
    public void onDestroy() {
        disposable.clear();
        super.onDestroy();
    }




}
