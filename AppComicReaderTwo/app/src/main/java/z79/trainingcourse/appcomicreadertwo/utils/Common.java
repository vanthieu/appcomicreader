package z79.trainingcourse.appcomicreadertwo.utils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

import z79.trainingcourse.appcomicreadertwo.roomdb.LocalStoryDataSource;

public class Common {

//    public static final String API_COMIC_ENPOINT = "http://10.0.2.2:3000/";
public static final String API_COMIC_ENPOINT = "https://appcomicreader.herokuapp.com/";
    public static final int REQUEST_PERMISSION = 9999 ;
    public static final int DEFAULT_COLUMN = 0 ;
    public static final int FULL_WITH_COLUMN = 1 ;
    public static final String KEY_USER = "keyUser" ;
    public static String USER_ID = "userID" ;


    public static String API_KEY = "";
    public static final int VIEW_TYPE_ITEM = 0 ;
    public static final int VIEW_TYPE_LOADING = 1 ;
    public static int currentIDStory;
    public static boolean currentIsComic;
    public static LocalStoryDataSource localStoryDataSource;
    public static int currentIDCategory;
    public static int currentIDStoryCheck;


    public static boolean isValidPassword(final String password) {

        Pattern pattern;
        Matcher matcher;
        final String PASSWORD_PATTERN = "^(?=.*[0-9])(?=.*[A-Z])(?=.*[@#$%^&+=!])(?=\\S+$).{4,}$";
        pattern = Pattern.compile(PASSWORD_PATTERN);
        matcher = pattern.matcher(password);

        return matcher.matches();

    }

    public static String buildJWT(String apiKey) {

        return new StringBuilder("Bearer")
                .append(" ")
                .append(apiKey).toString();
    }
}
