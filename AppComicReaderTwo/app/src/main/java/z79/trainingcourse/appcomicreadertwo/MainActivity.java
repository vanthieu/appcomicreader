package z79.trainingcourse.appcomicreadertwo;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.google.android.gms.auth.api.Auth;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;
import com.google.android.gms.auth.api.signin.GoogleSignInResult;
import com.google.android.material.bottomnavigation.BottomNavigationView;


import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;

import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;

import io.reactivex.schedulers.Schedulers;
import z79.trainingcourse.appcomicreadertwo.fragment.FragmentAccount;
import z79.trainingcourse.appcomicreadertwo.fragment.FragmentCategory;
import z79.trainingcourse.appcomicreadertwo.fragment.FragmentHome;

import z79.trainingcourse.appcomicreadertwo.fragment.FragmentLibrary;
import z79.trainingcourse.appcomicreadertwo.model.Categories;
import z79.trainingcourse.appcomicreadertwo.model.Story;
import z79.trainingcourse.appcomicreadertwo.model.event.EventLoadStoryByCategoryId;
import z79.trainingcourse.appcomicreadertwo.retrofit.APIComic;
import z79.trainingcourse.appcomicreadertwo.retrofit.RetrofitClient;
import z79.trainingcourse.appcomicreadertwo.utils.Common;
import z79.trainingcourse.appcomicreadertwo.utils.SaveKey;

import static com.facebook.FacebookSdk.getApplicationContext;

public class MainActivity extends AppCompatActivity {

    @BindView(R.id.layout_title_actionBar)
    public LinearLayout layout_title_actionBar;
    @BindView(R.id.txt_title_actionBar)
    TextView txt_title_actionBar;

    @BindView(R.id.layout_search_actionBar)
    public LinearLayout layout_search_actionBar;
    @BindView(R.id.imgbtn_back_actionBar)
    public ImageButton imgbtn_back_actionBar;
    @BindView(R.id.edt_search_actionBar)
    public EditText edt_search_actionBar;

    @BindView(R.id.layout_button_actionBar)
    public LinearLayout layout_button_actionBar;
    @BindView(R.id.imgbtn_search_actionBar)
    ImageButton imgbtn_search_actionBar;
    @BindView(R.id.imgbtn_sync_actionBar)
    ImageButton imgbtn_sync_actionBar;

    @BindView(R.id.nav_view)
    BottomNavigationView nav_view;

    Fragment fragment;
    FragmentManager manager;
    FragmentHome fragmentHome;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        ButterKnife.bind(this);


        init();
    }



    private void callMethodFromFragmentHome() {
        manager = getSupportFragmentManager();
        fragmentHome = (FragmentHome) manager.findFragmentById(R.id.framlayout_host);

    }

    private void init() {
        eventSearch();
        eventEditTextSearch();
        eventBackSearch();
        eventBottomNavitionBar();
        eventSyncStory();


    }

    private void eventSyncStory() {
        imgbtn_sync_actionBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                callMethodFromFragmentHome();
                fragmentHome.saveData();
            }
        });
    }


    private void eventBackSearch() {
        imgbtn_back_actionBar.setOnClickListener(view -> {

            layout_title_actionBar.setVisibility(View.VISIBLE);
            layout_search_actionBar.setVisibility(View.GONE);
            layout_button_actionBar.setVisibility(View.VISIBLE);

            callMethodFromFragmentHome();
            fragmentHome.backSearch();
        });
    }

    private void eventEditTextSearch() {
        edt_search_actionBar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {

            }

            @Override
            public void afterTextChanged(Editable editable) {
                callMethodFromFragmentHome();
                fragmentHome.searchText(editable);
            }
        });
    }

    private void eventBottomNavitionBar() {
        //set FragmentHome is main
        loadFragment(new FragmentHome());
        nav_view.setOnNavigationItemSelectedListener(item -> {

            int id = item.getItemId();
            switch (id) {
                case R.id.nav_home:
                    fragment = new FragmentHome();
                    txt_title_actionBar.setText(R.string.txt_home);
                    layout_button_actionBar.setVisibility(View.VISIBLE);
                    loadFragment(fragment);
                    return true;

                case R.id.nav_category:
                    fragment = new FragmentCategory();
                    txt_title_actionBar.setText(R.string.txt_category);
                    layout_button_actionBar.setVisibility(View.GONE);
                    loadFragment(fragment);
                    return true;
                case R.id.nav_library:
                    txt_title_actionBar.setText(R.string.txt_library);
                    layout_button_actionBar.setVisibility(View.GONE);
                    checkSessionUser();
                    return true;
                case R.id.nav_account:
                    fragment = new FragmentAccount();
                    txt_title_actionBar.setText(R.string.txt_account);
                    layout_button_actionBar.setVisibility(View.GONE);
                    loadFragment(fragment);
                    return true;
            }
            return false;
        });

    }

    private void checkSessionUser() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        SaveKey saveKey = new SaveKey(this);
        if (saveKey.readKey(Common.USER_ID) != ""){
            Toast.makeText(this, "Success In", Toast.LENGTH_SHORT).show();
            fragment = new FragmentLibrary();
            loadFragment(fragment);
        }else if (AccessToken.getCurrentAccessToken() != null){
            Toast.makeText(this, "Success In", Toast.LENGTH_SHORT).show();
            fragment = new FragmentLibrary();
            loadFragment(fragment);
        }else if (account!= null){
            Toast.makeText(this, "Success In", Toast.LENGTH_SHORT).show();
            fragment = new FragmentLibrary();
            loadFragment(fragment);
        }
        else {
            Toast.makeText(this, "Bạn cần đăng nhập để xem", Toast.LENGTH_LONG).show();
        }

    }


    private void eventSearch() {
        imgbtn_search_actionBar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                displaySearchBar();
            }
        });
    }

    private void displaySearchBar() {
        layout_title_actionBar.setVisibility(View.GONE);
        layout_button_actionBar.setVisibility(View.GONE);

        layout_search_actionBar.setVisibility(View.VISIBLE);

    }


    private void loadFragment(Fragment fragment) {
        FragmentTransaction transaction = getSupportFragmentManager().beginTransaction();
        transaction.replace(R.id.framlayout_host, fragment);
        transaction.addToBackStack(null);
        transaction.commit();

    }



    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
//        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        switch (requestCode){
            case Common.REQUEST_PERMISSION:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED){

                    Toast.makeText(MainActivity.this, "Thanks u for allow", Toast.LENGTH_SHORT).show();
                    callMethodFromFragmentHome();
                    fragmentHome.checkPermissionReadStore();
                    fragmentHome.saveData();

                }else {
                    AlertDialog.Builder alertDialog = new AlertDialog.Builder(this);
                    alertDialog.setMessage("Annouce allow permission to save story while you reading");
                    alertDialog.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            callMethodFromFragmentHome();
                            fragmentHome.checkPermissionReadStore();
                            fragmentHome.saveData();
                            Toast.makeText(MainActivity.this, "Thanks u to allow", Toast.LENGTH_SHORT).show();
                        }
                    });
                    alertDialog.setNegativeButton("No", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            dialogInterface.dismiss();
                        }
                    });
                    alertDialog.show();
                }

                break;
            default:
                break;
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, @Nullable Intent data) {
        fragment.onActivityResult(requestCode, resultCode, data);
        super.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        loadFragment(new FragmentHome());

    }

}
