package z79.trainingcourse.appcomicreadertwo.retrofit;



import io.reactivex.Observable;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.POST;
import retrofit2.http.Query;
import z79.trainingcourse.appcomicreadertwo.model.AllStoryModel;
import z79.trainingcourse.appcomicreadertwo.model.CategoryModel;
import z79.trainingcourse.appcomicreadertwo.model.ChapterModel;
import z79.trainingcourse.appcomicreadertwo.model.LinkModel;
import z79.trainingcourse.appcomicreadertwo.model.LoginUserModel;
import z79.trainingcourse.appcomicreadertwo.model.SearchModel;
import z79.trainingcourse.appcomicreadertwo.model.Story;
import z79.trainingcourse.appcomicreadertwo.model.StoryModel;
import z79.trainingcourse.appcomicreadertwo.model.UserModel;
import z79.trainingcourse.appcomicreadertwo.model.Users;

public interface APIComic {

    @GET("getStoryLimit")
    Observable<StoryModel> getStoryLimit(@Query("from") int from,
                                       @Query("to") int to);

    @GET("getAllStory")
    Observable<AllStoryModel> getAllStory();

    @POST("searchStory")
    @FormUrlEncoded
    Observable<SearchModel> searchStory(@Field("Name") String name);

    @GET("getCategoryByStoryID")
    Observable<CategoryModel> getCategoryByStoryID(@Query("storyID") int storyID);

    @GET("getAllCategory")
    Observable<CategoryModel> getAllCategory();

    @GET("getStoryByCategoryID")
    Observable<StoryModel> getStoryByCateGoryID(@Query("categoryID") int categoryID);

    @GET("getChapterByStoryID")
    Observable<ChapterModel> getChapterByStoryID(@Query("storyID") int storyID);

    @GET("getContentStoryByChapterID")
    Observable<LinkModel> getContentStoryByChapterID(@Query("ChapterID") int ChapterID);

    @POST("login")
    @FormUrlEncoded
    Observable<LoginUserModel> loginUser(@Field("Email") String Email,
                                         @Field("password") String password);

    @POST("newUser")
    @FormUrlEncoded
    Observable<UserModel> newUser(@Field("userName") String userName,
                                  @Field("Email") String Email,
                                  @Field("password") String password);


}
