package z79.trainingcourse.appcomicreadertwo.model.event;

import z79.trainingcourse.appcomicreadertwo.model.Categories;

public class EventLoadStoryByCategoryId {

    private boolean success;
    Categories categories;

    public EventLoadStoryByCategoryId() {
    }

    public EventLoadStoryByCategoryId(boolean success, Categories categories) {
        this.success = success;
        this.categories = categories;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public Categories getCategories() {
        return categories;
    }

    public void setCategories(Categories categories) {
        this.categories = categories;
    }
}
