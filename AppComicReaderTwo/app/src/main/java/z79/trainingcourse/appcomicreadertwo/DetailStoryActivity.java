package z79.trainingcourse.appcomicreadertwo;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.facebook.AccessToken;
import com.google.android.gms.auth.api.signin.GoogleSignIn;
import com.google.android.gms.auth.api.signin.GoogleSignInAccount;

import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.disposables.CompositeDisposable;
import io.reactivex.schedulers.Schedulers;
import jp.wasabeef.glide.transformations.BitmapTransformation;
import jp.wasabeef.glide.transformations.BlurTransformation;
import z79.trainingcourse.appcomicreadertwo.fragment.FragmentAccount;
import z79.trainingcourse.appcomicreadertwo.fragment.FragmentLibrary;
import z79.trainingcourse.appcomicreadertwo.model.Categories;
import z79.trainingcourse.appcomicreadertwo.model.Story;
import z79.trainingcourse.appcomicreadertwo.model.event.EventListStory;
import z79.trainingcourse.appcomicreadertwo.retrofit.APIComic;
import z79.trainingcourse.appcomicreadertwo.retrofit.RetrofitClient;
import z79.trainingcourse.appcomicreadertwo.roomdb.Favorite;
import z79.trainingcourse.appcomicreadertwo.roomdb.IStoryDataSource;
import z79.trainingcourse.appcomicreadertwo.roomdb.LocalStoryDataSource;
import z79.trainingcourse.appcomicreadertwo.roomdb.StoryDatabase;
import z79.trainingcourse.appcomicreadertwo.utils.Common;
import z79.trainingcourse.appcomicreadertwo.utils.SaveKey;

import static java.lang.Integer.parseInt;

public class DetailStoryActivity extends AppCompatActivity {

    @BindView(R.id.img_bg_detailStory)
    ImageView img_bg_detailStory;
    @BindView(R.id.imgbtn_back_detailStory)
    ImageButton imgbtn_back_detailStory;
    @BindView(R.id.img_avatar_detailStory)
    ImageView img_avatar_detailStory;

    @BindView(R.id.txt_name_detailStory)
    TextView txt_name_detailStory;
    @BindView(R.id.txt_author_detailStory)
    TextView txt_author_detailStory;
    @BindView(R.id.txt_type_detailStory)
    TextView txt_type_detailStory;
    @BindView(R.id.txt_status_detailStory)
    TextView txt_status_detailStory;
    @BindView(R.id.txt_numOfChapter_detailStory)
    TextView txt_numOfChapter_detailStory;
    @BindView(R.id.txt_dayUpload_detailStory)
    TextView txt_dayUpload_detailStory;

    @BindView(R.id.btn_read_detailStory)
    Button btn_read_detailStory;
    @BindView(R.id.txt_content_detailStory)
    TextView txt_content_detailStory;

    SimpleDateFormat simpleDateFormat;


    CompositeDisposable disposable;
    APIComic mservice;
    IStoryDataSource iStoryDataSource;


    List<Categories> listcategory;
    private Story storylist;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detail_story);
        simpleDateFormat = new SimpleDateFormat("YYYY/MM/dd");
        ButterKnife.bind(this);
        mservice = RetrofitClient.getRetrofit(Common.API_COMIC_ENPOINT).create(APIComic.class);
        disposable = new CompositeDisposable();
        iStoryDataSource = new LocalStoryDataSource(StoryDatabase.getInstance(this).iStoryDAO());

//        checksession();

        init();

    }

    private void init() {
        eventBackLayoutDetail();
        eventReadStory();
    }

    private void eventReadStory() {
        btn_read_detailStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                AlertDialog.Builder alert = new AlertDialog.Builder(DetailStoryActivity.this);
                alert.setTitle("Lưu Truyện");
                alert.setMessage("Truyện sẽ được lưu sau khi đăng nhập !");
                alert.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });
                alert.setNegativeButton("Đọc Truyện", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();

                        startActivity(new Intent(DetailStoryActivity.this, ReadStoryActivity.class));

                        checksession();
                    }
                });

                alert.show();
//                    startActivity(new Intent(DetailStoryActivity.this,ReadStoryActivity.class));
//                    saveStory();

            }
        });
    }

    private void checksession() {
        GoogleSignInAccount account = GoogleSignIn.getLastSignedInAccount(this);
        SaveKey saveKey = new SaveKey(this);
        if (saveKey.readKey(Common.USER_ID) != "") {
            saveStory();
        } else if (AccessToken.getCurrentAccessToken() != null) {
            saveStory();
        } else if (account != null) {
            saveStory();
        } else {
//            Toast.makeText(this, "read without save story", Toast.LENGTH_LONG).show();

        }
    }

    private void eventBackLayoutDetail() {
        imgbtn_back_detailStory.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                finish();
            }
        });
    }


    private void saveStory() {
        Favorite favorite = new Favorite();
        favorite.setID(storylist.getID());
        favorite.setName(storylist.getName());
        favorite.setImage(storylist.getImage());
        favorite.setNumOfChap(storylist.getNumOfChap());

        disposable.add(iStoryDataSource.insertFavoriteOrReplace(favorite)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(() -> {
                    Toast.makeText(this, "SAVE DATA SUCCESS", Toast.LENGTH_SHORT).show();
                }, throwable -> {
                    Toast.makeText(this, "[CAN NOT SAVEDATA]", Toast.LENGTH_SHORT).show();
                    Log.d("CANTSAVEDATA", throwable.getMessage().toString());
                }));

    }

    @Subscribe(sticky = true, threadMode = ThreadMode.MAIN)
    public void detailStory(EventListStory eventListStory) {
        if (eventListStory.isSuccess()) {
//            Toast.makeText(this, ""+ eventListStory.getStory().getName(), Toast.LENGTH_SHORT).show();
            Common.currentIDStory = eventListStory.getStory().getID();
            Common.currentIsComic = eventListStory.getStory().isComic();
            Common.currentIDStoryCheck = eventListStory.getStory().getID();

            storylist = eventListStory.getStory();

            Glide.with(this)
                    .load(eventListStory.getStory().getImage())
                    .apply(RequestOptions.bitmapTransform(new BlurTransformation(15)))
                    .into(img_bg_detailStory);
            Glide.with(this)
                    .load(eventListStory.getStory().getImage())
                    .into(img_avatar_detailStory);

            txt_name_detailStory.setText("" + eventListStory.getStory().getName());
            txt_author_detailStory.setText("Tác Giả : " + eventListStory.getStory().getAuthor());

            disposable.add(mservice.getCategoryByStoryID(eventListStory.getStory().getID())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribeOn(Schedulers.io())
                    .subscribe(categoryModel -> {
                        if (categoryModel.isSuccess()) {
                            listcategory = new ArrayList<>();
                            listcategory.addAll(categoryModel.getResult());

                            StringBuilder type = new StringBuilder();
                            type.append("Thể Loại : ");
                            for (Categories c : listcategory) {
                                type.append(c.getName() + ", ");
//                        type = c.getName();
//                        Log.d("name",type);
                            }
                            txt_type_detailStory.setText(type);

                        }
                    }, throwable -> {
                        Toast.makeText(this, "Vui lòng kết nối internet để hiển thị thể loại" , Toast.LENGTH_SHORT).show();
                        Log.d("CANTCATEGORY", throwable.getMessage());
                    }));

            StringBuilder status = new StringBuilder();
            status.append("Trạng Thái : ");
            if (eventListStory.getStory().getStatus().equals("loading")) {
                status.append("Đang cập nhật");
            } else {
                status.append("Đủ bộ");
            }
            txt_status_detailStory.setText(status);

            txt_numOfChapter_detailStory.setText("Tổng Chương :" + eventListStory.getStory().getNumOfChap());

            txt_dayUpload_detailStory.setText("Ngày Đăng : " + simpleDateFormat.format(eventListStory.getStory().getDate_Upload()));

            txt_content_detailStory.setText("Nội Dung : " + "\n" + eventListStory.getStory().getDescription());

        }
    }

    @Override
    protected void onStart() {
//        EventBus.getDefault().register(this);

        if (!EventBus.getDefault().isRegistered(this)) {
            EventBus.getDefault().register(this);
        }
        super.onStart();
    }

    @Override
    protected void onDestroy() {
        EventBus.getDefault().unregister(this);
        disposable.clear();
        super.onDestroy();
    }


}
