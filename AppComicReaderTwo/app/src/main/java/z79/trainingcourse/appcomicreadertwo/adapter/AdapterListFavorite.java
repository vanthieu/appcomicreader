package z79.trainingcourse.appcomicreadertwo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import z79.trainingcourse.appcomicreadertwo.DetailStoryActivity;
import z79.trainingcourse.appcomicreadertwo.R;
import z79.trainingcourse.appcomicreadertwo.interfaces.IClickListener;
import z79.trainingcourse.appcomicreadertwo.roomdb.Favorite;
import z79.trainingcourse.appcomicreadertwo.utils.Common;

public class AdapterListFavorite extends RecyclerView.Adapter<AdapterListFavorite.MyViewHolder> {

    Context context;
    List<Favorite> favoriteList;

    public AdapterListFavorite(Context context, List<Favorite> favoriteList) {
        this.context = context;
        this.favoriteList = favoriteList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_layout_list_favorite,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(favoriteList.get(position).getImage()).into(holder.circleimg_avatar_fav);
        holder.txt_name_fav.setText("Tên Truyện : " + favoriteList.get(position).getName());
        holder.txt_numOfChapter_fav.setText("Số Chương : " + favoriteList.get(position).getNumOfChap());

        holder.setListener(new IClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                Common.currentIDStory = favoriteList.get(position).getID();
                context.startActivity(new Intent(context, DetailStoryActivity.class));
            }
        });

    }

    @Override
    public int getItemCount() {
        return favoriteList.size();
    }



    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Unbinder unbinder;

        @BindView(R.id.circleimg_avatar_fav)
        CircleImageView circleimg_avatar_fav;
        @BindView(R.id.txt_name_fav)
        TextView txt_name_fav;
        @BindView(R.id.txt_numOfChapter_fav)
        TextView txt_numOfChapter_fav;
        @BindView(R.id.view_background)
        public RelativeLayout view_background;
        @BindView(R.id.view_foreground)
        public LinearLayout view_foreground;

        IClickListener listener;

        public void setListener(IClickListener listener) {
            this.listener = listener;
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this,itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            listener.itemClickListener(view,getAdapterPosition());
        }
    }

    public void removeItem(int position){
        favoriteList.remove(position);
        notifyItemRemoved(position);
    }

    public void restoreItem(Favorite favorite,int position){
        favoriteList.add(position,favorite);
        notifyItemInserted(position);
    }
}
