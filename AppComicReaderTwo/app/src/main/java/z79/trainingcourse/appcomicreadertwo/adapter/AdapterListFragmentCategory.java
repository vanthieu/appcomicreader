package z79.trainingcourse.appcomicreadertwo.adapter;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.RecyclerView;

import org.greenrobot.eventbus.EventBus;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import z79.trainingcourse.appcomicreadertwo.MainActivity;
import z79.trainingcourse.appcomicreadertwo.R;
import z79.trainingcourse.appcomicreadertwo.StoryByCategoryActivity;
import z79.trainingcourse.appcomicreadertwo.fragment.FragmentHome;
import z79.trainingcourse.appcomicreadertwo.interfaces.IClickListener;
import z79.trainingcourse.appcomicreadertwo.model.Categories;
import z79.trainingcourse.appcomicreadertwo.model.event.EventLoadStoryByCategoryId;
import z79.trainingcourse.appcomicreadertwo.utils.Common;

public class AdapterListFragmentCategory extends RecyclerView.Adapter<AdapterListFragmentCategory.MyViewHolder> {

    Context context;
    List<Categories> listCategory;

    public AdapterListFragmentCategory(Context context, List<Categories> listCategory) {
        this.context = context;
        this.listCategory = listCategory;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_fragment_list_category, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        holder.txt_name_category.setText(listCategory.get(position).getName());

        holder.setiClickListener(new IClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
//                Toast.makeText(context, ""+listCategory.get(position).getName(), Toast.LENGTH_SHORT).show();
                Common.currentIDCategory = listCategory.get(position).getID();
                EventBus.getDefault().postSticky(new EventLoadStoryByCategoryId(true,listCategory.get(position)));
//                openHomeFragmentDisplayCategory();
                context.startActivity(new Intent(context, StoryByCategoryActivity.class));
                Log.d("IDCATEGORY",listCategory.get(position).getID()+"");


            }
        });
    }

//    private void openHomeFragmentDisplayCategory() {
//        if (context.getClass().equals(MainActivity.class)){
//            FragmentManager manager = ((MainActivity)context).getSupportFragmentManager();
//            FragmentTransaction transaction = manager.beginTransaction();
//            FragmentHome fragmentHome = new FragmentHome();
//            transaction.replace(R.id.framlayout_host,fragmentHome);
//            transaction.commit();
//        }
//    }

    @Override
    public int getItemCount() {
        return listCategory.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        @BindView(R.id.txt_name_category)
        TextView txt_name_category;

        Unbinder unbinder;
        IClickListener iClickListener;

        public void setiClickListener(IClickListener iClickListener) {
            this.iClickListener = iClickListener;
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);

        }

        @Override
        public void onClick(View view) {
            iClickListener.itemClickListener(view,getAdapterPosition());
        }
    }

    @Override
    public int getItemViewType(int position) {
        if (listCategory.size() == 1) {
            return Common.DEFAULT_COLUMN;
        } else {
            if (listCategory.size() % 2 == 0)
                return Common.DEFAULT_COLUMN;
            else
                return (position > 1 && position == listCategory.size() - 1) ? Common.FULL_WITH_COLUMN : Common.DEFAULT_COLUMN;
        }

    }
}
