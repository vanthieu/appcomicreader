package z79.trainingcourse.appcomicreadertwo.roomdb;

import android.content.Context;

import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;

import z79.trainingcourse.appcomicreadertwo.model.Story;

@Database(version = 1,entities = {Story.class,Favorite.class,Content.class},exportSchema = false)
public abstract class StoryDatabase extends RoomDatabase {

    public abstract IStoryDAO iStoryDAO();

    private static StoryDatabase storyDatabase;

    public static StoryDatabase getInstance(Context context){
        if (storyDatabase == null)
            storyDatabase = Room.databaseBuilder(context,StoryDatabase.class,"StoryDB")
                    .allowMainThreadQueries()
                    .build();

        return storyDatabase;
    }
}
