package z79.trainingcourse.appcomicreadertwo.model;

import java.util.List;

public class AllStoryModel {

    boolean success;
    String message;
    List<AllStory> result;


    public AllStoryModel(boolean success, String message, List<AllStory> result) {
        this.success = success;
        this.message = message;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<AllStory> getResult() {
        return result;
    }

    public void setResult(List<AllStory> result) {
        this.result = result;
    }
}
