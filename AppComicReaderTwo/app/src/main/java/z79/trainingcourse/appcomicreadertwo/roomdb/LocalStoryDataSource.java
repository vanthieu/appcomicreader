package z79.trainingcourse.appcomicreadertwo.roomdb;

import java.util.List;

import io.reactivex.Completable;
import io.reactivex.Flowable;
import z79.trainingcourse.appcomicreadertwo.model.Story;

public class LocalStoryDataSource implements IStoryDataSource {

    private IStoryDAO iStoryDAO;


    public LocalStoryDataSource(IStoryDAO iStoryDAO) {
        this.iStoryDAO = iStoryDAO;
    }

    @Override
    public Completable insertStoryOrReplace(List<Story> storyList) {
        return iStoryDAO.insertStoryOrReplace(storyList);
    }

    @Override
    public Flowable<List<Story>> getAllListStory() {
        return iStoryDAO.getAllListStory();
    }

    @Override
    public Completable insertFavoriteOrReplace(Favorite favoriteList) {
        return iStoryDAO.insertFavoriteOrReplace(favoriteList);
    }

    @Override
    public Flowable<List<Favorite>> getFavorite() {
        return iStoryDAO.getFavorite();
    }

    @Override
    public Completable insertContentOrReplace(Content contentList) {
        return iStoryDAO.insertContentOrReplace(contentList);
    }

    @Override
    public Flowable<List<Content>> getContentList() {
        return iStoryDAO.getContentList();
    }


    @Override
    public void insertFav(Favorite... favorites) {
        iStoryDAO.insertFav(favorites);
    }

    @Override
    public void delete(Favorite... favorites) {
        iStoryDAO.delete(favorites);
    }


}
