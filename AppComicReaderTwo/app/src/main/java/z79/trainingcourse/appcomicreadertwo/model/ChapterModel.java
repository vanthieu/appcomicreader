package z79.trainingcourse.appcomicreadertwo.model;

import java.util.List;

public class ChapterModel {

    private boolean success;
    private String message;
    private List<Chapter> result;

    public ChapterModel() {
    }

    public ChapterModel(boolean success, String message, List<Chapter> result) {
        this.success = success;
        this.message = message;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Chapter> getResult() {
        return result;
    }

    public void setResult(List<Chapter> result) {
        this.result = result;
    }
}
