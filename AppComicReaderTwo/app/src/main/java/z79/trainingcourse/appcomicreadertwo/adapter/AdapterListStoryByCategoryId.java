package z79.trainingcourse.appcomicreadertwo.adapter;

import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import org.greenrobot.eventbus.EventBus;

import java.text.SimpleDateFormat;
import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import de.hdodenhof.circleimageview.CircleImageView;
import z79.trainingcourse.appcomicreadertwo.DetailStoryActivity;
import z79.trainingcourse.appcomicreadertwo.R;
import z79.trainingcourse.appcomicreadertwo.interfaces.IClickListener;
import z79.trainingcourse.appcomicreadertwo.model.Story;
import z79.trainingcourse.appcomicreadertwo.model.event.EventListStory;
import z79.trainingcourse.appcomicreadertwo.utils.Common;

public class AdapterListStoryByCategoryId extends RecyclerView.Adapter<AdapterListStoryByCategoryId.MyViewHolder> {

    Context context;
    List<Story> listStoryByCategoryId;
    SimpleDateFormat simpleDateFormat;


    public AdapterListStoryByCategoryId(Context context, List<Story> listStoryByCategoryId) {
        this.context = context;
        this.listStoryByCategoryId = listStoryByCategoryId;
        simpleDateFormat = new SimpleDateFormat("YYYY/MM/dd");
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_layout_list_story_by_categoryid,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {

        Glide.with(context).load(listStoryByCategoryId.get(position).getImage()).into(holder.circleimg_avatar_listStoryByCategoryId);

        holder.txt_name_listStoryByCategoryId.setText(listStoryByCategoryId.get(position).getName());

        String status = listStoryByCategoryId.get(position).getStatus();
        StringBuilder statusStory = new StringBuilder();
        statusStory.append("Trạng Thái : ");
        if (status.equals("loading")) {
            statusStory.append("Đang Cập Nhật");
        } else {
            statusStory.append("Đủ Bộ");
        }
        holder.txt_status_listStoryByCategoryId.setText(statusStory);

        holder.txt_author_listStoryByCategoryId.setText(listStoryByCategoryId.get(position).getAuthor());

        holder.txt_day_update_listStoryByCategoryId.setText(new StringBuilder("Ngày Cập Nhật : " + simpleDateFormat.format(listStoryByCategoryId.get(position).getDate_Update())));

        holder.setClickListener(new IClickListener() {
            @Override
            public void itemClickListener(View view, int position) {
                Common.currentIDStory = listStoryByCategoryId.get(position).getID();
                Common.currentIsComic = listStoryByCategoryId.get(position).isComic();
                EventBus.getDefault().postSticky(new EventListStory(true,listStoryByCategoryId.get(position)));
                context.startActivity(new Intent(context, DetailStoryActivity.class));

            }
        });
    }

    @Override
    public int getItemCount() {
        return listStoryByCategoryId.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {

        Unbinder unbinder;

        @BindView(R.id.circleimg_avatar_listStoryByCategoryId)
        CircleImageView circleimg_avatar_listStoryByCategoryId;
        @BindView(R.id.txt_name_listStoryByCategoryId)
        TextView txt_name_listStoryByCategoryId;
        @BindView(R.id.txt_status_listStoryByCategoryId)
        TextView txt_status_listStoryByCategoryId;
        @BindView(R.id.txt_author_listStoryByCategoryId)
        TextView txt_author_listStoryByCategoryId;
        @BindView(R.id.txt_day_update_listStoryByCategoryId)
        TextView txt_day_update_listStoryByCategoryId;

        IClickListener clickListener;

        public void setClickListener(IClickListener clickListener) {
            this.clickListener = clickListener;
        }

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this, itemView);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View view) {
            clickListener.itemClickListener(view,getAdapterPosition());
        }
    }
}
