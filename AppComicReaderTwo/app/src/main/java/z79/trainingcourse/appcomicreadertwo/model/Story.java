package z79.trainingcourse.appcomicreadertwo.model;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.room.TypeConverters;

import java.util.Date;

import z79.trainingcourse.appcomicreadertwo.utils.DateConverter;

@Entity(tableName = "Story")
public class Story {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "ID")
    public int ID;
    @ColumnInfo(name = "Name")
    public String Name;
    @ColumnInfo(name = "Image")
    public String Image;
    @ColumnInfo(name = "Status")
    public String Status;
    @ColumnInfo(name = "Description")
    public String Description;
    @ColumnInfo(name = "Author")
    public String Author;
    @ColumnInfo(name = "NumOfChap")
    public int NumOfChap;
    @ColumnInfo(name = "Date_Upload")
    @TypeConverters(DateConverter.class)
    public Date Date_Upload;
    @ColumnInfo(name = "Date_Update")
    @TypeConverters(DateConverter.class)
    public Date Date_Update;
    @ColumnInfo(name = "IsComic")
    public boolean IsComic;


    public Story() {
    }

    public Story(int ID, String name, String image, String status, String description, String author, int numOfChap, Date date_Upload, Date date_Update, boolean isComic) {
        this.ID = ID;
        Name = name;
        Image = image;
        Status = status;
        Description = description;
        Author = author;
        NumOfChap = numOfChap;
        Date_Upload = date_Upload;
        Date_Update = date_Update;
        IsComic = isComic;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getImage() {
        return Image;
    }

    public void setImage(String image) {
        Image = image;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    public String getDescription() {
        return Description;
    }

    public void setDescription(String description) {
        Description = description;
    }

    public String getAuthor() {
        return Author;
    }

    public void setAuthor(String author) {
        Author = author;
    }

    public int getNumOfChap() {
        return NumOfChap;
    }

    public void setNumOfChap(int numOfChap) {
        NumOfChap = numOfChap;
    }

    public Date getDate_Upload() {
        return Date_Upload;
    }

    public void setDate_Upload(Date date_Upload) {
        Date_Upload = date_Upload;
    }

    public Date getDate_Update() {
        return Date_Update;
    }

    public void setDate_Update(Date date_Update) {
        Date_Update = date_Update;
    }

    public boolean isComic() {
        return IsComic;
    }

    public void setComic(boolean comic) {
        IsComic = comic;
    }
}
