package z79.trainingcourse.appcomicreadertwo.interfaces;

import android.view.View;

public interface IClickListener {

    void itemClickListener(View view,int position);
}
