package z79.trainingcourse.appcomicreadertwo.model;

public class AllStory {

    int maxRowNum;

    public AllStory() {
    }

    public AllStory(int maxRowNum) {
        this.maxRowNum = maxRowNum;
    }

    public int getMaxRowNum() {
        return maxRowNum;
    }

    public void setMaxRowNum(int maxRowNum) {
        this.maxRowNum = maxRowNum;
    }
}
