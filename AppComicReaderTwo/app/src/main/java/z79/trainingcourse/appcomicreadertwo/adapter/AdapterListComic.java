package z79.trainingcourse.appcomicreadertwo.adapter;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.bumptech.glide.Glide;

import java.util.List;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.Unbinder;
import z79.trainingcourse.appcomicreadertwo.R;
import z79.trainingcourse.appcomicreadertwo.ReadStoryActivity;
import z79.trainingcourse.appcomicreadertwo.model.Link;
import z79.trainingcourse.appcomicreadertwo.roomdb.Content;

public class AdapterListComic extends RecyclerView.Adapter<AdapterListComic.MyViewHolder> {

    Context context;
    List<Link> listLinkComic;
    List<Content> contentList;

    public AdapterListComic(Context context, List<Link> listLinkComic) {
        this.context = context;
        this.listLinkComic = listLinkComic;
    }

    public AdapterListComic(ReadStoryActivity context, List<Content> contentList) {
        this.context = context;
        this.contentList = contentList;
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        return new MyViewHolder(LayoutInflater.from(context).inflate(R.layout.custom_layout_list_comic,parent,false));
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Glide.with(context).load(listLinkComic.get(position).getLink()).into(holder.img_comic);
    }

    @Override
    public int getItemCount() {
        return listLinkComic.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.img_comic)
        ImageView img_comic;

        Unbinder unbinder;
        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            unbinder = ButterKnife.bind(this,itemView);
        }
    }
}
