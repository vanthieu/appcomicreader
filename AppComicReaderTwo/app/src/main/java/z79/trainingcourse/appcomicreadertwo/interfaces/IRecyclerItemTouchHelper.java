package z79.trainingcourse.appcomicreadertwo.interfaces;

import androidx.recyclerview.widget.RecyclerView;

public interface IRecyclerItemTouchHelper {
    void onSwiped(RecyclerView.ViewHolder holder,int direction,int position);
}
