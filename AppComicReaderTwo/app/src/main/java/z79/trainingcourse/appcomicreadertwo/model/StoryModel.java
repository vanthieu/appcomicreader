package z79.trainingcourse.appcomicreadertwo.model;

import java.util.List;

public class StoryModel {

    boolean success;
    String message;
    List<Story> result;

    public StoryModel() {
    }

    public StoryModel(boolean success, String message, List<Story> result) {
        this.success = success;
        this.message = message;
        this.result = result;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<Story> getResult() {
        return result;
    }

    public void setResult(List<Story> result) {
        this.result = result;
    }
}
